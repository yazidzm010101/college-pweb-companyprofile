<?php
require_once __DIR__ . '/server/const.php';
?>

<html>
<?php include_once __DIR__ . '/server/template/header.php'; ?>
<link rel="stylesheet" href="<?= BASEURL ?>/public/css/common.css">
<header>
    <?php include_once __DIR__ . '/server/template/navbar.php'; ?>
</header>

<body>
    <h1 class="text-center text-white">404 - Not Found</h1>
</body>

<?php include_once __DIR__ . '/server/template/footer.php'; ?>
<link rel="stylesheet" href="<?= BASEURL ?>/public/css/about.css" async>

</html>