<?php
require_once __DIR__ . '/server/const.php';
require_once __DIR__ . '/server/database/database_posts.php';
$id = (isset($_GET['id']) ? $_GET['id'] : 1);
$post = postGet($id);
?>

<?php
if (empty($post)) {
    header("Location: " . BASEURL . "/index.php");
} else {
?>

    <html>
    <?php include_once __DIR__ . '/server/template/header.php'; ?>
    <header>
        <?php include_once __DIR__ . '/server/template/navbar.php'; ?>
    </header>

    <body>
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-lg-8">
                        <section id="hero-img">
                            <img src="<?= BASEURL ?>/public/posts/default_thumb.jpg" class="img-fluid" alt="Responsive image">
                        </section>
                        <section id="post-content">
                            <?php include_once __DIR__ . '/public/posts/' . $post['post_id'] . '/index.html'; ?>
                        </section>
                    </div>
                    <div class="d-none d-md-block col-md-5 col-lg-4">
                        <div class="custom-title custom-title-left mb-4 text-left text-light">
                            <div class="custom-title-text">OTHER NEWS</div>
                        </div>
                        <?php include_once __DIR__ . '/server/template/newscardlist.php'; ?>
                    </div>
                </div>
            </div>
        </article>

        <footer>
            <div class="container">
                <p>© Copyright Gen-Z 2020</p>
            </div>
        </footer>
    </body>
    <?php include_once __DIR__ . '/server/template/footer.php'; ?>
    <link rel="stylesheet" href="<?= BASEURL ?>/public/css/post.css">
    <script>
        h1 = $('#post-content h1').html().toUpperCase();
        $('#post-content h1').remove();
        $('#post-content').prepend(
            `<div class="custom-title custom-title-left mb-4 text-left text-light">
                <div class="custom-title-text">${h1}</div>
            </div>`);
        navswitch(0);
    </script>

    </html>

<?php } ?>