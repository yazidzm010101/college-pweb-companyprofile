<?php
require_once __DIR__ . '/../database/database_posts.php';
$posts = postGetAll();
?>

<?php foreach ($posts as $post) {
?>
    <div class="card mt-3 mb-3">
        <img src="<?= BASEURL ?>/public/posts/<?= $post['post_id']  ?>/thumb.jpg" class="card-img-top" alt="..." onerror="this.src='<?= BASEURL ?>/public/posts/default_thumb.jpg';">
        <small class="card-footer text-right text-muted"><?= $post['post_date'] ?></small>
        <div class="card-body">
            <h5 class="card-title"><?= $post['post_title'] ?></h5>
            <small class="card-text text-muted"><?= $post['post_desc'] ?></small>
        </div>
        <div class="card-body mb-2">
            <a href="<?= BASEURL ?>/posts.php?id=<?= $post['post_id']  ?>" class="card-link card-link-custom-primary">Learn more</a>
        </div>
    </div>
<?php } ?>