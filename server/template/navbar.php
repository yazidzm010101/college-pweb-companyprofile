<nav class="navbar fixed-top navbar-expand-lg navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="">Gen-Z</a>
        <button class="navbar-toggler third-button " type="button" data-toggle="collapse" data-target="#navbarSupportedContent22" aria-controls="navbarSupportedContent22" aria-expanded="false" aria-label="Toggle navigation">
            <div class="animated-icon3"><span></span><span></span><span></span></div>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent22">
            <div class="navbar-nav mr-auto">
            </div>
            <div class="nav navbar-nav navbar-right">
                <a href="<?= BASEURL ?>/index.php" class="nav-item nav-link">News</a>
                <a href="<?= BASEURL ?>/about.php" class="nav-item nav-link">About</a>
                <a href="#" class="nav-item nav-link">Our Team</a>
                <a href="#" class="nav-item nav-link">Contact</a>
            </div>
        </div>
    </div>
</nav>