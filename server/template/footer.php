<script src="<?= BASEURL ?>/public/js/jquery.min.js"></script>
<script src="<?= BASEURL ?>/public/js/popper.min.js"></script>
<script src="<?= BASEURL ?>/public/js/bootstrap.min.js"></script>
<script>
    function navswitch(id) {
        $('.nav-link.active').removeClass('active');
        $('.nav-link:eq(' + id + ')').addClass('active');
    }

    function scrollto(id) {
        $('html, body').animate({
            scrollTop: $('#'+id).offset().top - $('#'+id).outerHeight()/3,
        }, 500);        
    }
</script>