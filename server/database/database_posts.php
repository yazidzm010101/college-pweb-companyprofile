<?php

function postGetAll()
{
    require __DIR__ . '/database.php';
    $sql = $pdo->query("SELECT * FROM posts");
    $posts = $sql->fetchAll(PDO::FETCH_ASSOC);
    return $posts;
}

function postGet($id = 1)
{
    require __DIR__ . '/database.php';
    $sql = $pdo->prepare("SELECT * FROM posts WHERE post_id = ?");
    $sql->bindValue(1, $id);
    $sql->execute();
    $post = $sql->fetch(PDO::FETCH_ASSOC);
    return $post;
}
