<?php require_once __DIR__ . '/server/const.php'; ?>
<html lang="en">
<?php include_once __DIR__ . '/server/template/header.php'; ?>

<body>
    <!-- NAVBAR -->
    <header>
        <?php include_once __DIR__ . '/server/template/navbar.php'; ?>
    </header>

    <!-- HERO -->
    <article id="hero">
        <section id="hero-img">
            <img src="<?= BASEURL ?>/public/img/base.png" class="img-fluid" alt="Responsive image">
        </section>
        <section id="hero-body" class="text-center text-light">
            <h1>Gen-Z</h1>
            <p>more than just a game</p>
        </section>
        <section id="hero-footer"  class="text-center">
            <div class="arrow-down" onclick="scrollto('news')">
                <span class="arrow-left"></span><span class="arrow-right"></span>
            </div>
        </section>
    </article>

    <!-- NEWS -->
    <article id="news">
        <?php include_once __DIR__ . '/server/template/newscard.php' ?>
    </article>

    <!-- FOOTER -->
    <footer>
        <div class="container">
            <p>© Copyright Gen-Z 2020</p>
        </div>
    </footer>
</body>

<?php include_once __DIR__ . '/server/template/footer.php'; ?>
<link rel="stylesheet" href="<?= BASEURL ?>/public/css/index.css" async>
<script>
    navswitch(0);
</script>

</html>