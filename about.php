<?php require_once __DIR__ . '/server/const.php'; ?>

<html>
<?php include_once __DIR__ . '/server/template/header.php'; ?>

<body>
    <!-- NAVBAR -->
    <header>
        <?php include_once __DIR__ . '/server/template/navbar.php'; ?>
    </header>

    <!-- HERO -->
    <article id="hero">
        <section id="hero-img">
            <img src="<?= BASEURL ?>/public/img/base.png" class="img-fluid" alt="Responsive image">
        </section>
        <section id="hero-body" class="text-center text-light">
            <h1>Gen-Z</h1>
            <p>more than just a game</p>
        </section>
    </article>

    <!-- COMPANY DESCRIPTION -->
    <article id="desc" class="pt-5 pb-5">
        <div class="container">
            <div class="custom-title custom-title-left mb-4 text-left text-light">
                <div class="custom-title-text">WHO ARE WE?</div>
            </div>
            <div class="row ml-md-1">
                <div class="col-md-4 col-lg-3 mt-1 mb-1 d-flex flex-column justify-content-center align-items-center align-items-md-start">
                    <section class="section-icon">
                        <img src="<?= BASEURL ?>/public/img/genz-circle.png" alt="Gen-Z Circle" class="img-fluid">
                    </section>
                </div>
                <div class="col-md-8 col-lg-9 d-flex flex-column justify-content-center align-items-center">
                    <section class="section-text text-light">
                        <p>Gen-Z merupakan sebuah startup game studio yang didirikan pada 30 Januari 2020. Kami terinspirasi untuk memberikan edukasi melalui media game kepada generasi milenial.</p>
                    </section>
                </div>
            </div>
        </div>
    </article>

    <!-- GAME ENGINES -->
    <article id="geng" class="pt-5 pb-5">
        <div class="container">
            <div class="custom-title custom-title-left mb-4 text-left text-light">
                <div class="custom-title-text">GAME ENGINES</div>
            </div>
            <div class="row ml-md-1">
                <div class="col-md-4 col-lg-3 mt-1 mb-1 d-flex flex-column justify-content-center align-items-center align-items-md-start order-md-2">
                    <section class="section-icon">
                        <img src="<?= BASEURL ?>/public/img/game-engines.png" alt="Gen-Z Circle" class="img-fluid">
                    </section>
                </div>
                <div class="col-md-8 col-lg-9 d-flex flex-column justify-content-center align-items-center order-md-1">
                    <section class="section-text text-light">
                        <p>Dengan game engine yang sudah modern, kami memproduksi game untuk berbagai platform dari Windows, MacOs, Linux, Android, dan iOs.</p>
                    </section>
                </div>
            </div>
        </div>
    </article>

    <!-- TEAM -->
    <article id="team" class="pt-5 pb-5">
        <div class="container">
            <div class="custom-title custom-title-left mb-4 text-left text-light">
                <div class="custom-title-text">TEAM</div>
            </div>
            <div class="row ml-md-1">
                <div class="col-md-4 col-lg-3 mt-1 mb-1 d-flex flex-column justify-content-center align-items-center align-items-md-start">
                    <section class="section-icon">
                        <img src="<?= BASEURL ?>/public/img/team-circle.png" alt="Gen-Z Circle" class="img-fluid">
                    </section>
                </div>
                <div class="col-md-8 col-lg-9 d-flex flex-column justify-content-center align-items-center">
                    <section class="section-text text-light">
                        <p>Kami memiliki tim yang professional dan berdedikasi tinggi kami untuk menciptakan produk yang berkualitas dan memiliki daya saing tinggi.</p>
                    </section>
                </div>
            </div>
        </div>
    </article>

    <!-- CONTACT US -->
    <article id="ctus" class="text-center">
        <div class="container">
            <p class="text-light">Jangan sungkan untuk menghubungi kami, kami bersedia memberikan layanan yang terbaik untuk anda.</p>
            <a href="#" class="btn btn-custom-primary">Contact Us</a>
        </div>
    </article>

    <!-- FOOTER -->
    <footer>
        <div class="container">
            <p>© Copyright Gen-Z 2020</p>
        </div>
    </footer>
</body>

<?php include_once __DIR__ . '/server/template/footer.php'; ?>
<link rel="stylesheet" href="<?= BASEURL ?>/public/css/about.css" async>
<script>
    navswitch(1);
</script>

</html>